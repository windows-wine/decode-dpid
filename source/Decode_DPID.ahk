; version 1.3
; Drugwash, 2013-2023
;================================================================
;@Ahk2Exe-Obey bits, = %A_PtrSize% * 8
;@Ahk2Exe-Obey xbit, = "%A_PtrSize%" > 4 ? "x64" : "x86"
;@Ahk2Exe-Obey type, = "%A_IsUnicode%" ? "Unicode" : "ANSI"
;@Ahk2Exe-Obey xtype, = "%A_IsUnicode%" ? "W" : "A"
;@Ahk2Exe-Let version = 1.3.1.1
;@Ahk2Exe-AddResource *14 %A_ScriptDir%\res\DPID.ico, 159
;@Ahk2Exe-AddResource *14 %A_ScriptDir%\res\DPID.ico, 207
;@Ahk2Exe-SetFileVersion %U_version%
;@Ahk2Exe-SetProductVersion 1.3.0.0
;@Ahk2Exe-SetProductName Decode DPID
;@Ahk2Exe-SetInternalName Decode DPID %U_version%.ahk
;@Ahk2Exe-SetOrigFilename Decode DPID.exe
;@Ahk2Exe-SetDescription Decode DPID - Retrieve/decode digital product ID for MS Windows
;@Ahk2Exe-SetCompanyName Drugwash Hobby Programming
;@Ahk2Exe-SetCopyright Copyright @ Drugwash`, Apr 2013-Aug 2023
;@Ahk2Exe-SetLegalTrademarks Released under the terms of the GNU General Public License
;@Ahk2Exe-Set Comments, Compiled in AHK %A_AhkVersion% %U_type% %U_bits%bit.
;@Ahk2Exe-SetMainIcon %A_ScriptDir%\res\DPID.ico
;@Ahk2Exe-UpdateManifest 0, Drugwash.HobbyProgramming.DecodeDPID, %U_version%
;================================================================
#NoEnv
#SingleInstance Force
ListLines, Off
SetBatchLines -1
SetControlDelay, -1
SetWinDelay, -1
DetectHiddenWindows, On
updates()
;================================================================
author = Drugwash
appname = Decode DPID
version = 1.3.1.1 %AW%
releaseD = August 17, 2023
releaseT = private
;================================================================
; generic variables
dispVars := "pn,vn,wv,id,flg,ins,own,org,pid,pid1,res,lic,keyt,dvd"	; variables for display
cv := "Full package,Compliance checked,OEM,Volume License"	; channel version, zero-based
it := "Compact,Typical,Portable,Custom"	; InstallType, zero-based
; GUI
Gui, Font, s8 w400, Tahoma
Gui, Add, DropDownList, x10 y16 w170 h21 R6 AltSubmit vsList gselSource
	, current system registry||remote system registry*|exported registry file
	|binary file|clipboard data|manual input
Gui, Add, Button, x182 y16 w24 h21, ...
Gui, Add, Button, x208 y16 w50 h21, Go
Gui, Add, GroupBox, x4 y42 w550 h236, Registry information
Gui, Add, GroupBox, x4 y280 w550 h214, DigitalProductId (raw)
Gui, Add, GroupBox, x4 y2 w258 h38, Source
Gui, Add, Text, x10 y56 w130 h16, Operating system:
Gui, Add, Text, x10 y72 w130 h16, OS version:
Gui, Add, Text, x10 y88 w130 h16, OS version (API):
Gui, Add, Text, x10 y104 w130 h16, Installation date:
Gui, Add, Text, x10 y120 w130 h16, Setup flags:
Gui, Add, Text, x10 y136 w130 h16, Installation type:
Gui, Add, Text, x10 y156 w130 h16, Registered owner:
Gui, Add, Text, x10 y172 w130 h16, Registered organization:
Gui, Add, Text, x10 y188 w130 h16, Product ID:
Gui, Add, Text, x10 y204 w130 h16, Product key:
Gui, Add, Text, x10 y222 w130 h16, Licence info:
Gui, Add, Text, x10 y240 w130 h16, Key type:
Gui, Add, Text, x10 y258 w130 h16, DVD region:
Gui, Add, Button, x266 y16 h21 Hidden vopn gsel3, Open .reg
Gui, Add, Button, x410 y16 h21 gsaveFile, Save to file
Gui, Add, Button, x+5 y16 w50 h21 gGuiClose, Exit
Gui, Add, Edit, x140 y56 w410 h16 -Multi -E0x200 ReadOnly vpn,
Gui, Add, Edit, x140 y72 w410 h16 -Multi -E0x200 ReadOnly vvn,
Gui, Add, Edit, x140 y88 w410 h16 -Multi -E0x200 ReadOnly vwv,
Gui, Add, Edit, x140 y104 w410 h16 -Multi -E0x200 ReadOnly vid,
Gui, Add, Edit, x140 y120 w410 h16 -Multi -E0x200 ReadOnly vflg,
Gui, Add, Edit, x140 y136 w410 h16 -Multi -E0x200 ReadOnly vins,
Gui, Add, Edit, x140 y156 w410 h16 -Multi -E0x200 ReadOnly vown,
Gui, Add, Edit, x140 y172 w410 h16 -Multi -E0x200 ReadOnly vorg,
Gui, Add, Edit, x140 y188 w200 h16 -Multi -E0x200 ReadOnly vpid,
Gui, Add, Edit, x+0 y188 w210 h16 -Multi -E0x200 ReadOnly vpid1,
Gui, Font, Bold, Tahoma
Gui, Add, Edit, x140 y204 w410 h16 -Multi -E0x200 ReadOnly vres,
Gui, Font, Norm, Tahoma
Gui, Add, Edit, x140 y222 w410 h16 -Multi -E0x200 ReadOnly vlic,
Gui, Add, Edit, x140 y240 w410 h16 -Multi -E0x200 ReadOnly vkeyt,
Gui, Add, Edit, x140 y258 w410 h16 -Multi -E0x200 ReadOnly vdvd,
;Gui, Add, Text, x266 y16 w136 h21 vident,
Gui, Font, s7 w400, Tahoma
Gui, Add, ListView, x8 y294 w542 R11 -0x4000000 vLV1
	, 00|01|02|03|04|05|06|07|08|09|0A|0B|0C|0D|0E|0F|-
; Generated using SmartGuiXP Creator mod 4.3.29.8
Gui, Show, Center w558, %appname% %version%

selSource:
Gui, Submit, NoHide
SetTimer, sel%sList%, -1
return
Return
;================================================================
GuiClose:
FileDelete, %tmpReg%
ExitApp
;================================================================
sel1:
GuiControl, Hide, opn
gosub regRead
gosub calc
goto dispData
return

sel3:
GuiControl, Show, opn
gosub openFile
if !savReg
	return
gosub savRead
gosub calc
goto dispData
return

sel2:
; see https://docs.microsoft.com/en-us/windows/win32/sysinfo/registry-functions
; RegConnectRegistry()
sel4:
sel5:
sel6:
msgbox, Not implemented yet.
return
;================================================================
regRead:
skey := A_OSType="WIN32_NT" ? " NT" : "", isXP := A_OSVersion="WIN_XP" ? true : false
RegRead, pn, HKLM, Software\Microsoft\Windows%skey%\CurrentVersion, ProductName
RegRead, ostrg, HKLM, Software\Microsoft\Windows%skey%\CurrentVersion, DigitalProductID
RegRead, pid, HKLM, Software\Microsoft\Windows%skey%\CurrentVersion, ProductID
RegRead, own, HKLM, Software\Microsoft\Windows%skey%\CurrentVersion, RegisteredOwner
RegRead, org, HKLM, Software\Microsoft\Windows%skey%\CurrentVersion, RegisteredOrganization
RegRead, li, HKLM, Software\Microsoft\Windows%skey%\CurrentVersion
	, % w9x ? "LicensingInfo" : "LicenseInfo"
RegRead, inst, HKLM, Software\Microsoft\Windows%skey%\CurrentVersion, "InstallType"
RegRead, setF, HKLM, Software\Microsoft\Windows%skey%\CurrentVersion, "SetupFlags"
RegRead, dvd, HKLM, Software\Microsoft\Windows%skey%\CurrentVersion, "DVD_Region"
RegRead, verN, HKLM, Software\Microsoft\Windows%skey%\CurrentVersion
	, % w9x ? "VersionNumber" : "CurrentVersion"
RegRead, sverN, HKLM, Software\Microsoft\Windows%skey%\CurrentVersion
	, % w9x ? "SubVersionNumber" : isXP ? "CurrentBuildNumber" : "CurrentBuild"
id := w9x ? getInstDate9x() : getInstDateNT()
return
;================================================================
savRead:
section := "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion"
IniRead, sect, %savReg%, %section%
if !sect
	{
	tmpReg := A_Temp "\tmpRegFile.reg"
	FileRead, tR, *c %savReg%
	t := StrGet(&tR,, "CP0")
	FileDelete, %tmpReg%
	FileAppend, %t%, %tmpReg%, UTF-16
	savReg := tmpReg
	Sleep, 1000
	Sleep, -1
	section := "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion"
	sav9x := true
	}
else
	{
	IniRead, ost, %savReg%, %section%, "CurrentBuildNumber", %A_Space%
	isXP := ost ? true : false, sav9x := false
	}
IniRead, pn, %savReg%, %section%, "ProductName", %A_Space%
IniRead, pid, %savReg%, %section%, "ProductID", %A_Space%
IniRead, own, %savReg%, %section%, "RegisteredOwner", %A_Space%
IniRead, org, %savReg%, %section%, "RegisteredOrganization", %A_Space%
IniRead, dvd, %savReg%, %section%, "DVD_Region", %A_Space%
if sav9x
	{
	IniRead, inst, %savReg%, %section%, "InstallType", %A_Space%
	IniRead, setF, %savReg%, %section%, "SetupFlags", %A_Space%
	IniRead, verN, %savReg%, %section%, "VersionNumber", %A_Space%
	IniRead, sverN, %savReg%, %section%, "SubVersionNumber", %A_Space%
	IniRead, li, %savReg%, %section%, "LicensingInfo", %A_Space%
	IniRead, idt, %savReg%, %section%, "FirstInstallDateTime", %A_Space%
	StringReplace, idt, idt, hex:,,
	StringReplace, idt, idt, `,,, All
	StringReplace, flg, setF, hex:,,
	StringReplace, flg, flg, `,, ., All
	}
else
	{
	flg := inst := ""
	IniRead, verN, %savReg%, %section%, "CurrentVersion", %A_Space%
	IniRead, sverN, %savReg%, %section%, "CurrentBuildNumber", %A_Space%
	if !sverN
		IniRead, sverN, %savReg%, %section%, "CurrentBuild", %A_Space%
	IniRead, li, %savReg%, %section%, "LicenseInfo", %A_Space%
	IniRead, idt, %savReg%, %section%, "InstallDate", %A_Space%
	StringReplace, idt, idt, dword:, 0x
	}
StringReplace, li, li, hex:,,
StringReplace, li, li, `,, ., All
StringReplace, li, li, \,, All
StringReplace, li, li, `n,, All
StringUpper, li, li

if !r := GetRegStrg(savReg, "DigitalProductID", ostrg)
	msgbox, Error in GetRegStrg(). Need another reg key.
;msgbox, r=%r%`n%ostrg%

id := sav9x ? getInstDate9x(,, idt) : getInstDateNT(,, idt)
return
;================================================================
calc:
; Create main binary buffer
VarSetCapacity(mainbuf, StrLen(ostrg)/2, 0)
Loop, % StrLen(ostrg)/2
	NumPut("0x" SubStr(ostrg, 2*A_Index-1, 2), mainbuf, A_Index-1, "UChar")
sl := StrLen(ostrg)
; Find data length
datalen := NumGet(mainbuf, 0, "UInt")
; Find product version
Loop, 2
	prodver%A_Index% := NumGet(mainbuf, 2*(A_Index-1)+4, "UShort")
prodver := prodver1 "." prodver2
; Retrieve Product ID
pid1 := DllCall("MulDiv", Ptr, &mainbuf+8, "Int", 1, "Int", 1, AStr)
fpid := pid1 ? pid1 : pid	; failsafe when digital PID is null - ex. in Wine
; Unknown item 1
u1 := NumGet(mainbuf, 32, "UInt")
; Unknown item 2
u2 := DllCall("MulDiv", Ptr, &mainbuf+36, "Int", 1, "Int", 1, AStr)
; Unknown item 3
u3 := NumGet(mainbuf, 68, "UInt")
; Calculate channel
if prodver=3
	{
	chnval := NumGet(mainbuf, 80, "UInt")
	Loop, Parse, cv, CSV
		if (chnval=A_Index-1)
			chn := A_LoopField
	}
else if prodver=4
	{
	chnval=
	Loop, 128
		chnval .= NumGet(mainbuf, A_Index+1015, "UChar")	; start at offset 1016, 128 bytes, Unicode
	chn := chnval Chr(0) Chr(0)
	; WideCharToMultiByte
	}
else chn := "Unknown"
; Calculate install type
Loop, Parse, it, CSV
	if (A_Index = inst+1)
		ins := A_LoopField
; Calculate version number
vn := verN ((w9x OR sav9x) ? "" : ".") sverN
res := GetPK(ostrg, 0)	; only for 32bit, for 64bit set par2 to 1
wv := sList < 3? GetOSver(true) : "<not available from file>"
/*
lic := ""
Loop, Parse, li, .
lic .= Chr("0x" A_LoopField)
*/
lic := li	; this is to be analyzed
return
;================================================================
openFile:
FileSelectFile, savReg, 3, %A_ScriptDir%\
	, Select exported registry file:, Registry Files (*.reg)
if (!savReg OR ErrorLevel)
	return
return
;================================================================
saveFile:
;=== This is for display only ===
dOstrg := ""
Loop, % Ceil(sl / 32)
	{
	StringMid, s, ostrg, 1+32*(A_Index-1), 32
	Loop, 16
		{
		StringMid, c, s, 1+2*(A_Index-1), 2
		if (c="")
			break
		dOstrg .= c "."
		}
	StringTrimRight, dOstrg, dOstrg, 1
	dOstrg .= "`n"
	}
StringTrimRight, dOstrg, dOstrg, 1
;===
FileSelectFile, outfile, S24, %A_ScriptDir%\%A_ComputerName%_DPID.txt
	, Select destination path:, Text Documents (*.txt)
if (!outfile OR ErrorLevel)
	return
Loop, %outfile%
	if !A_LoopFileExt
		outfile .= ".txt"
dv := sList < 3 ? wv : vn	; save printed version when read from file
FileDelete, %outfile%
FileAppend,
(
Operating system: %pn%
OS version: %dv%
Channel: %chn%
Installed on: %id%
Product Key: %res%
Readable Product ID: %pid%
Decoded Product ID: %pid1%

Data length: %datalen%
Product version: %prodver%
unknown1: %u1%
unknown2: %u2%
unknown3: %u3%

Raw DigitalProductId:
%dOstrg%
), %outfile%
return
;================================================================
dispData:
Loop, Parse, dispVars, CSV
	GuiControl,, %A_LoopField%, % %A_LoopField%
LV_Delete()
GuiControl, -Redraw, LV1
Loop, % Ceil(sl / 32)
	{
	StringMid, s, ostrg, 1+32*(A_Index-1), 32
	Loop, 16
		{
		StringMid, c%A_Index%, s, 1+2*(A_Index-1), 2
		if (c%A_Index% = "")
			break
		}
	LV_Add("", c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16)
	c1 := c2 := c3 := c4 := c5 := c6 := c7 := c8 := c9
	:= c10 := c11 := c12 := c13 := c14 := c15 := c16 := ""
	}
GuiControl, +Redraw, LV1
return
;================================================================
GetRegStrg(lpFileName, lpKeyName, ByRef res)
{
Global
rex1 := """" lpKeyName """=hex:"
FileRead, var, %lpFileName%
r := RegExMatch(var, "imsxU)(*BSR_ANYCRLF)" rex1 "\K.*(?="")", res)
StringReplace, res, res, \,, All
StringReplace, res, res, %A_Space%,, All
StringReplace, res, res, `n,, All
StringReplace, res, res, `r,, All
StringReplace, res, res, `,,, All
StringUpper, res, res
return r
}

#include lib\func_GetOSver.ahk
#include lib\func_GetOSInstDate.ahk
#include lib\func_GetProdKey.ahk
