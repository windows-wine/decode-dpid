; Retrieves OS install date according to OS type - Win9x or NT line
; requires updates() from updates.ahk
; � Drugwash, 2021.05.30 - 2022.02.22

getInstDateNT(dFormat="", tFormat="", fDate="")
{
Static rpath := "Software\Microsoft\Windows NT\CurrentVersion"
	, rkey := "InstallDate"
if !fDate
	RegRead, fDate, HKLM, %rpath%, %rkey%
i := 19700101000000
i += fDate, s
if !(dFormat && tFormat)
	FormatTime, t, %i% R D2
else FormatTime, t, %i%, %dFormat%, %tFormat%
return t
}
;================================================================
getInstDate9x(dFormat="", tFormat="", bDate="")
;================================================================
{
Global A_CharSize, Ptr
Static rpath := "Software\Microsoft\Windows\CurrentVersion"
	, rkey := "FirstInstallDateTime"
if !bDate
	RegRead, bDate, HKLM, %rpath%, %rkey%
fDate := "0x" SubStr(bDate, 7, 2) SubStr(bDate, 5, 2)
fTime := "0x" SubStr(bDate, 3, 2) SubStr(bDate, 1, 2)

VarSetCapacity(FILETIME, 8, 0)	; FILETIME struct
if !DllCall("kernel32\DosDateTimeToFileTime"
	, "UShort", fDate
	, "UShort", fTime
	, Ptr, &FILETIME)
	MsgBox, Error converting date/time
else
	{
	VarSetCapacity(SYSTEMTIME, 16, 0)	; SYSTEMTIME struct
	DllCall("kernel32\FileTimeToSystemTime"
		, Ptr, &FILETIME
		, Ptr, &SYSTEMTIME)
	}
if rSz := GetDateFormat(&SYSTEMTIME, dFormat, oDate:="", 0)
	{
	VarSetCapacity(oDate, rSz*A_CharSize, 0)	;twice capacity when it's being run with a Unicode AHK
	GetDateFormat(&SYSTEMTIME, dFormat, oDate, rSz)
	}
else oDate := "date error " A_LastError
if rSz := GetTimeFormat(&SYSTEMTIME, tFormat, oTime:="", 0)
	{
	VarSetCapacity(oTime, rSz*A_CharSize, 0)	; same as above
	GetTimeFormat(&SYSTEMTIME, tFormat, oTime, rSz)
	}
else oTime := "time error " A_LastError

Loop, 7
	v%A_Index% := (A_Index = 1)
	? NumGet(SYSTEMTIME, 2*(A_Index-1), "UShort")
	: SubStr("00" NumGet(SYSTEMTIME, 2*(A_Index-1), "UShort"), -1)
d := dFormat ? v1 "." v2 "." v4 : oDate
t := tFormat ? v5 ":" v6 ":" v7 : oTime
return d ", " t
}
;================================================================
GetDateFormat(in, fmt, ByRef out, sz)
{
	Global
	return DllCall("kernel32\GetDateFormat" AW
		, "UInt", 0x400			; LOCALE_SYSTEM_DEFAULT=0x800
		, "UInt", fmt ? 0: 0x2	; DATE_LONGDATE=0x2
		, "UInt", in
		, "UInt", fmt ? &fmt : 0
		, "Str", out
		, "UInt", sz)
}
;================================================================
GetTimeFormat(in, fmt, ByRef out, sz)
{
	Global
	return DllCall("kernel32\GetTimeFormat" AW
		, "UInt", 0x400			; LOCALE_SYSTEM_DEFAULT=0x800
		, "UInt", fmt ? 0 : 0x8	; TIME_FORCE24HOURFORMAT=0x8
		, "UInt", in
		, "UInt", fmt ? &fmt : 0
		, "Str", out
		, "UInt", sz)
}
